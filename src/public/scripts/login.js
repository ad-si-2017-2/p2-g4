(function ($) {
    'use strict';

    var loginForm = document.querySelector('#loginForm');
    loginForm.addEventListener('submit', function (event) {
        event.preventDefault();
        $.post("/login", {
            nickname: document.querySelector('#nicknameModal').value,
            server: document.querySelector('#servidorModal').value,
            channel: document.querySelector('#canalModal').value
        }).done(function (data) {
            location.reload();
        });
        return false;
    });


}($));