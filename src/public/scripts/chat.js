(function ($) {
    'use strict';
    var websocket = new WebSocket(`ws://localhost:9000/`);
    $('.btn-enviar').on('click', function () {
        if ($('#mensagem').val()) {
            websocket.send(JSON.stringify({
                message: $('#mensagem').val()
            }));
        }
    })

    websocket.onmessage = function (evt) {
        $('#messages').append($('<li>').html(evt.data));
    };

    websocket.onerror = function (evt) {
        $('#messages').append($('<li>').text('<span style="color: red;">ERROR:</span> ' + evt.data));
    };
}($));