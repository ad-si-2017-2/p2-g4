const configuration = require('./configuration');

const path = require('path');
const express = require('express');
const ejs = require('ejs');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const app = express();
const http = require('http').Server(app);
const WebSocketServer = require('ws').Server;
const wss = new WebSocketServer({
    port: configuration.websocket
});

var irc = require('irc');

var proxies = {};

function proxy(id, server, nick, channel) {
    var cache = []; // cache de mensagens

    var iClient = new irc.Client(
        server,
        nick, { channels: [channel], });

    iClient.addListener('message' + channel, function (sender, message) {
        console.log(sender + ' => ' + channel + ': ' + message);
        cache.push({
            "timestamp": Date.now(),
            "nick": sender,
            "msg": message
        });
    });

    iClient.addListener('error', function (message) {
        console.log('error: ', message);
    });

    iClient.addListener('mode', function (message) {
        console.log('mode: ', message);
    });

    iClient.addListener('join', function (channel, nick, message) {
        var msg = [];

        if (this.nick == nick) {
            io.emit('newChannel', channel);
        }

        msg.push({
            "timestamp": Date.now(),
            "origin": channel,
            "nick": channel,
            "msg": nick + ' entrou no canal.'
        });
        ws.emit('mensagemCanal', msg);
        iClient.send("names", channel);
    });

    iClient.addListener('join#channel', function (nick, message) {
        console.log(nick + ' Entrou no canal: ' + JSON.stringify(message));
    });

    iClient.addListener('part', function (channel, nick, reason, message) {

        if (reason == undefined) {
            reason = "";
        } else {
            reason = 'Justificativa: ' + reason;
        }
        var msg = [];
        msg.push({
            "timestamp": Date.now(),
            "origin": channel,
            "nick": channel,
            "msg": nick + ' deixou o canal.' + reason
        });
        ws.emit('channelMessage', msg);
    });

    iClient.addListener('quit', function (nick, reason, channels, message) {
        var msg = []
        for (var i = 0; i <= channels.length - 1; i++) {
            msg.push({
                "timestamp": Date.now(),
                "origin": channels[i],
                "nick": channels[i],
                "msg": nick + 'deixou o server. Justificativa: ' + reason
            });
        }

        ws.emit('mensagemCanal', msg);
        console.log('NICK: ' + nick + ' deixou os seguintes canais: ' + channels + '. Justificativa: ' + JSON.stringify(reason));
    })

    iClient.addListener('nick', function (oldnick, newnick, channels, message) {
        console.log('NICK: ' + oldnick + ' mudou para ' + newnick);
        ws.emit('changeNick', newnick);
    });

    iClient.addListener('names', function (channel, nicks) {
        ws.emit("namesList", { 'channel': channel, 'list_names': Object.keys(nicks) });
    });
    iClient.once('registered', function (message) {
        iClient.opt.nick = message.args[0];
        var messageList = [];
        messageList.push({
            "timestamp": Date.now(),
            "origin": "Servidor",
            "nick": "Servidor",
            "msg": message.args[1]
        })

        ws.emit("motdResponse", messageList);
    });
    iClient.addListener('motd', function (motd) {
        var messageList = [];
        messageList.push({
            "timestamp": Date.now(),
            "origin": "servidor",
            "nick": "Servidor",
            "msg": motd
        })
        ws.emit("motdResponse", messageList);
    });

    proxies[id] = { "cache": cache, "irc_client": iClient };

    return proxies[id];
}

app.use(cookieParser());
app.use(bodyParser());
app.use(express.static('./src/public/scripts', {
    index: false
}));
app.set('view engine', 'html');
app.engine('html', ejs.renderFile);
app.set('views', path.join(__dirname, './public/views'));

app.get('/', function (req, res) {
    if (req.cookies.client_id) {
        res.render('chat');
    } else {
        res.render('login');
    }
});

app.post('/login', function (req, res) {
    const body = req.body;
    const clientID = configuration.proxyId();
    ircClient = proxy(clientID, req.body.server, req.body.nickname, req.body.channel);
    res.cookie('client_id', clientID, { maxAge: 900000000, httpOnly: true }).end();
});

//Esse comando envia as mensagens para todos os clientes
wss.broadcast = function broadcast(data) {
    wss.clients.forEach(function each(client) {
        client.send(JSON.stringify(data));
    });
};

//Abre as conexões
wss.on('connection', function (ws) {
    ws.on('message', function (msg) {
        data = JSON.parse(msg);
        // TODO: listen to commands here
        wss.broadcast({
            "username": "batata",
            "date": new Date().toISOString(),
            "message": "test",
            "server": "asdfas",
            "channel": "asdfasdfas"
        });
    });
});

//Ouvindo a porta
http.listen(configuration.server, function () {
    console.log(`Ouvindo na porta *: ${configuration.server}`);
});