const uuid = require('uuid/v4');

module.exports = {
    server: 8081,
    websocket: 9000,
    proxyId: uuid
};
