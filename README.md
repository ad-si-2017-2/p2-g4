# APLICAÇÕES DISTRIBUÍDAS - INF/UFG - 17/2
Repositorio para a disciplina de Aplicações Distribuídas do curso de Sistemas de Informação da Universidade Federal de Goiás.
## Projeto 2 - Web Sockets e Proxy
### Membros
* [Yago Zardo](https://gitlab.com/yagozardo) - Líder do Grupo e Desenvolvedor
* [Renan Gustavo](https://gitlab.com/Vouks) - Documentador

### Documentação
- Toda a documentação deste projeto pode ser acompanhada pela [**Wiki**](https://gitlab.com/ad-si-2017-2/p2-g4/wikis/home).

### Requisitos
- O objetivo do projeto 2 é desenvolver uma interface WEB utilizando WebSocket (Socket.io) para o protocolo IRC. Foi apresentado um protótipo inicial do projeto 3 utilizando REST, deverá ser modificado para suportar WebSocket

### Entregáveis Desejáveis:
* Manual do usuário: informando como utilizar o servidor; requisitos de hardware e software; como instalar, testar e configurar;
* Manual do desenvolvedor: lista de cada mensagem suportada, referencias (RFC), testes de execução (procedimentos em telnet, resultado esperado); lista de mensagens não suportadas; diagramas de sequencia, diagramas de atividade.
* Código-fonte do sistema com documentação de casos de testes.